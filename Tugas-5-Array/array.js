var batas = "========================="
console.log (batas+'\n'+'Soal 1'+'\n'+batas)
// soal 1
function range(startNum,finishNum) {
    var r = [];
    if(startNum<finishNum){
        for (i=startNum;i<=finishNum;i++) {
            r.push(i)
        }
        return r
    } else if(startNum>finishNum) {
        for (i=startNum;i>=finishNum;i--) {
            r.push(i)
        }
        return r
    } else {return -1}
    
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

// soal 2
console.log (batas+'\n'+'Soal 2'+'\n'+batas)

function rangeWithStep(startNum,finishNum,step) {
    var r = [];
    if(startNum<finishNum){
        for (i=startNum;i<=finishNum;i+=step) {
            r.push(i)
        }
        return r
    } else if(startNum>finishNum) {
        for (i=startNum;i>=finishNum;i-=step) {
            r.push(i)
        }
        return r
    } else {return "invalid"}
    
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// soal 3
console.log (batas+'\n'+'Soal 3'+'\n'+batas)

function sum(startNum,finishNum,step) {
    var r = [];
    var t = 0
    var d = step
    if (!step) {
        step=1
    }
    else {step}
    if(startNum<finishNum){
        for (i=startNum;i<=finishNum;i+=step) {
            t+=i;
        }
        return t
    } else if(startNum>finishNum) {
        for (i=startNum;i>=finishNum;i-=step) {
            t+=i;
        }
        return t
    }else if (startNum) {return startNum
    } else return 0
    
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


//soal 4
console.log (batas+'\n'+'Soal 4'+'\n'+batas)
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling (data){
    for (var i=0;i<data.length; i++) {
        var id = "Nomor Id : "+data [i][0]
        var nama = "Nama Lengkap : "+data [i][1]
        var ttl = "TTL : "+ data[i][2]+" "+ data [i][3]
        var hobi = "Hobi : "+data[i][4]

        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)
    }
}
 dataHandling(input)

 // soal 5
 console.log (batas+'\n'+'Soal 5'+'\n'+batas)

 function balikKata(kata) {
     var wrd = " "
     for (i=kata.length-1; i >=0; i--) {
         wrd+=kata[i]
     }
     return wrd
 }
 console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

// soal 6
console.log (batas+'\n'+'Soal 6'+'\n'+batas)

var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 
dataHandling2(input2);

function dataHandling2(data2) {
    var newData = data2
    var newName = data2[1] + " Elsharawy"
    var newProvince ="Provinsi " + data2[2]
    var gender ="Pria"
    var institusi = "SMA International Metro"

    newData.splice(1,1, newName)
    newData.splice(2,1,newProvince)
    newData.splice(4,1,gender,institusi)

    var arrDate = data2 [3]
    var newDate = arrDate.split("/")
    var monthNum = newDate[1]
    var monthname = " "
    
    switch(monthNum){
    case 1: monthname = 'Januari'; break;
    case 2: monthname = 'Februari'; break;
    case 3: monthname = 'Maret'; break;
    case 4: monthname = 'April'; break;
    case 5: monthname = 'Mei'; break;
    case 6: monthname = 'Juni'; break;
    case 7: monthname = 'Juli'; break;
    case 8: monthname = 'Agustus'; break;
    case 9: monthname = 'September'; break;
    case 10: monthname = 'Oktober'; break;
    case 11: monthname = 'November'; break;
    case 12: monthname = 'Desember'; break;
}

    var dateJoin = newDate.join("-")
    var dateArr = newDate.sort(function(value1,value2) {
        value2-value1
    })
    var editName=newName.slice(0,15)
    console.log(newData)
    console.log(monthname)
    console.log(dateArr)
    console.log(dateJoin)
    console.log(editName)
}