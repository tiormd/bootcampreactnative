import React from 'react'
import { 
    StyleSheet, 
    Text, 
    View, 
    Image, 
    TextInput, 
    Platform, 
    TouchableOpacity, 
    Button, 
    KeyboardAvoidingView, 
    ScrollView, 
    ColorPropType
} from 'react-native'

const Login = () => {
    return (
        <KeyboardAvoidingView
        behavior ={Platform.OS =="ios" ? "padding" : "height"}
        style={styles.container} 
        >
        <ScrollView>
            <View style={styles.containerView}>
                <Image style={styles.img} source={require('../Tugas13/assets/logo.png')}/>
                <Text style={styles.logintext}>Login</Text>
                <View style={styles.formInput}>
                    <Text style={styles.formtext}>Username</Text>
                    <TextInput style={styles.input}/>
                </View>
                <View style={styles.formInput}>
                    <Text style={styles.formtext}>Password</Text>
                    <TextInput style={styles.input} secureTextEntry={true}/>
                </View>
                <View style={styles.kotaklogin}>
                    <TouchableOpacity style={styles.btlogin}>
                        <Text style={styles.textbt}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={styles.autotext}>atau</Text>
                    <TouchableOpacity style={styles.btreg}>
                        <Text style={styles.textbt}>Daftar ?</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default Login

const styles = StyleSheet.create({
    container:{
        flex:1,
        
    }, 
    logintext:{
        fontSize: 24,
        marginTop: 30,
        textAlign:'center',
        color:'#003366',
        marginVertical:20,
    },
    formtext:{
        color:'#003366'
    }, autotext:{
        fontSize:20,
        color:'#3EC6FF'
    }, formInput:{
        marginHorizontal:41,
        marginVertical: 5,
        alignContent:'center',
    }, input:{
        height:40,
        borderColor:'#003366',
        padding:10,
        borderWidth:1
    }, btlogin:{
        alignItems:'center',
        backgroundColor:'#3EC6FF',
        padding:8,
        borderRadius:16,
        marginHorizontal:117,
        marginTop:32,
        textAlign:'center',
    }, textbt:{
        fontSize:18,
        lineHeight:28,
        fontWeight:'normal',
        color:'#FFF',
        textAlign:'center',
        fontFamily:'Roboto',
        fontStyle:'normal'
    }, autotext:{
        textAlign:'center',
        color:'#3EC6FF',
        fontSize:18,
        marginVertical:10
    }, btreg:{
        alignItems:'center',
        backgroundColor:'#003366',
        padding:8,
        borderRadius:16,
        marginHorizontal:117,
        textAlign:'center',
    }, img:{
        marginTop:10
    }
})
