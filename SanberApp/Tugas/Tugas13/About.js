import React from 'react'
import { 
    StyleSheet, 
    Text, 
    View, 
    TextInput, 
    Button, 
    ScrollView,
    SafeAreaView,} from 'react-native'
import { FontAwesome5 } from '@expo/vector-icons';

const About = () => {
    return (
        <SafeAreaView  style={styles.container}>
        <ScrollView>
            <View>
                <Text style={styles.judul}>Tentang Saya</Text>
                <FontAwesome5 
                name="user-circle" 
                size={200} solid 
                color="#EFEFEF" 
                style={styles.icon}/>
                <Text style={styles.nama}>Rian Aditio Ramadhan</Text>
                <Text style={styles.kerjaan}>Android Developer</Text>
                <View style={styles.kotak}>
                    <Text style={styles.juduldalam}>Portofolio</Text>
                    <View style={styles.kotakdalam}>
                        <View>
                            <FontAwesome5 name="gitlab"
                            size={40}
                            color='#3EC6FF'
                            style={styles.icon}/>
                            <Text style={styles.textdalam}>@tiormd</Text>
                        </View>
                        <View>
                            <FontAwesome5 name="github"
                            size={40}
                            color='#3EC6FF'
                            style={styles.icon}/>
                            <Text style={styles.textdalam}>@tiormd</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.kotak}>
                    <Text style={styles.juduldalam}>Hubungi Saya</Text>
                    <View style={styles.kotakdalamver}>
                        <View style={styles.kotakdalamhub}>
                            <View style={styles.fb}>
                                <FontAwesome5 name="facebook"                            
                                size={40}
                                color='#3EC6FF'
                                style={styles.icon}/>    
                            </View>  
                            <View style={styles.textName}> 
                                <Text style={styles.textdalam}>Rian Aditio Ramadhan</Text>
                            </View>      
                        </View>
                        <View style={styles.kotakdalamhub}>
                            <View>
                                <FontAwesome5 name="instagram"
                                size={40}
                                color='#3EC6FF'
                                style={styles.icon}/>   
                            </View>  
                            <View style={styles.textName}>
                            <Text style={styles.textdalam}>@tiormd</Text> 
                            </View>      
                        </View>
                        <View style={styles.kotakdalamhub}>
                            <View>
                                <FontAwesome5 name="twitter"
                                size={40}
                                color='#3EC6FF'
                                style={styles.icon}/>    
                            </View> 
                            <View style={styles.textName}>
                            <Text style={styles.textdalam}>@tiormd3</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </ScrollView>
        </SafeAreaView>
        
    )
}

export default About

const styles = StyleSheet.create({
    container:{
        marginTop:30,
        flex:1
    }, judul:{
        fontSize:36,
        fontWeight:'bold',
        color:'#003366',
        textAlign:'center'
    }, icon:{
        textAlign:'center',
    }, nama:{
        fontSize:24,
        fontWeight:'bold',
        color:'#003366',
        textAlign:'center'
    }, kerjaan:{
        fontSize:16,
        fontWeight:'bold',
        color:'#3EC6FF',
        textAlign:'center',
        marginBottom:7
    }, kotak:{
        borderColor:'blue',
        borderRadius:10,
        borderBottomColor:'#000',
        padding:5,
        backgroundColor:'#EFEFEF',
        marginBottom:9,
        marginHorizontal:10 
    }, kotakdalam:{
        borderTopWidth:1,
        borderTopColor:'#003366',
        flexDirection:'row',
        justifyContent:'space-around'
    }, kotakdalamver:{
        borderTopWidth:1,
        borderTopColor:'#003366',
        flexDirection:'column',
        justifyContent:'space-around'
    }, kotakdalamhub:{
        height:50,
        flexDirection:'row',
        justifyContent:'center',
        marginBottom:2
    }, juduldalam:{
        fontSize:18,
        color:'#003366',
    }, textdalam:{
        fontSize:12,
        fontWeight:'bold',
        color:'#003366',
        textAlign:'center'
    }, textName:{
        justifyContent:"center",
        marginLeft:10
    }, fb:{
        marginLeft:71
    }
})
