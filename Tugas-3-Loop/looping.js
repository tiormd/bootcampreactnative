var batas= "==============================="


console.log(batas + "\n" + "Soal 1 Looping While" + "\n" + batas)
// soal 1
console.log("Looping Pertama")
var loop1= 2;
while(loop1<=20) {console.log(loop1 + " - I love coding");
loop1 += 2;
}
console.log("Looping Kedua")
var loop2 = 20;
while(loop2>0) {console.log(loop2 + " - I will become a mobile developer");
loop2 -= 2;
}

console.log(batas+"\n"+"Soal 2 Looping For"+"\n"+batas)
// soal 2

for(var deret=1; deret <= 20; deret++) {
    if (deret % 2 == 0) {console.log(deret + " - Berkualitas")}
    else if (deret % 2 != 0 && deret % 3 ==0) {console.log(deret+" - I Love Coding")}
    else if (deret % 2 != 0) {console.log (deret + " - Santai")}
}

console.log(batas+"\n"+"Soal 3 Persegi Panjang #"+"\n"+batas)
// soal 3
var str=''
for(var pp=0; pp <4 ;pp++) {
    for(var pb= 0;pb <8;pb++) {
     str+='*';   
    }
    str+= '\n';
}
console.log(str);

console.log(batas+"\n"+"Soal 4 Membuat Tangga"+"\n"+batas)
// soal 4
var str=''
for(var pp=0; pp <7 ;pp++) {
    for(var pb= 0;pb <=pp;pb++) {
     str+='*';   
    }
    str+= '\n';
}
console.log(str);

console.log(batas+"\n"+"Soal 5 Papan Catur"+"\n"+batas)
// soal 5
var s=8
var b= ""

for (var i=0; i<s;i++) {
    for (var k=0; k<s;k++) {
        if ((i+k)%2==0) b += " ";
        else b += "#";
    }
    b += "\n";
}
console.log(b);