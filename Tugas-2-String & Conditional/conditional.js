// Soal 1

var nama = "John"
var peran = "Sniper"

if(nama == false && peran == false){
    console.log("Nama harus diisi!")
}else if(peran == false){
    console.log("Halo " + nama + " Pilih peranmu untuk memulai game!")
}else if(peran == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, "+ nama)
    console.log("Halo " + peran, nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
}else if(peran == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, "+ nama)
    console.log("Halo " + peran, nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
}else if(peran == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, "+ nama)
    console.log("Halo " + peran, nama + ", Kamu akan memakan mangsa setiap malam!")
} else {console.log("Peran tidak ditemukan!")
}

//Soal 2

var hari = 30
var bulan = 1
var tahun = 2020

switch(bulan){
    case 1: bulan = 'Januari'; break;
    case 2: bulan = 'Februari'; break;
    case 3: bulan = 'Maret'; break;
    case 4: bulan = 'April'; break;
    case 5: bulan = 'Mei'; break;
    case 6: bulan = 'Juni'; break;
    case 7: bulan = 'Juli'; break;
    case 8: bulan = 'Agustus'; break;
    case 9: bulan = 'September'; break;
    case 10: bulan = 'Oktober'; break;
    case 11: bulan = 'November'; break;
    case 12: bulan = 'Desember'; break;
}

var tanggal = hari+" "+bulan+" "+tahun;
//Code dibawah supaya tanggal yang dimasukkan masuk akal, tetapi saya tidak tahu cara menyerdahanakannya
if (tahun >= 1900 && tahun <= 2200) 
    {if ((bulan=='Januari'||bulan=='Maret'||bulan=='Mei'||bulan=='Juli'||bulan=='Agustus'||bulan=='Oktober'||bulan=='Desember') && hari <=31) 
    {console.log(tanggal)}
    else if ((bulan=='April'||bulan=='Juni'||bulan=='September'||bulan=='November')&& hari <=30) 
    {console.log(tanggal)}
    else if (bulan=='Februari' && hari <=29) {console.log(tanggal)}
    else {console.log("Tanggal/Bulan Invalid!")}
    }
else {
    console.log("Tahun Invalid!")
    }
