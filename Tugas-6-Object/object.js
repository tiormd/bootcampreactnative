var batas = "========================="
console.log (batas+'\n'+'Soal 1'+'\n'+batas)
// soal 1
function arrayToObject (arr){
    if(arr.length <=0){
        return console.log("")
    }

    for(i=0; i<arr.length; i++){
        var obj = {};
        obj.firstName = arr[i][0]
        obj.lastName = arr[i][1]
        obj.gender = arr[i][2]
        var thnLahir = arr[i][3];
        var now = new Date().getFullYear();
        var umur;
        if (thnLahir && now - thnLahir>0){
            umur=now-thnLahir
            } else {umur="Invalid Birth Year"}
        obj.age= umur
        
        var orang = (i+1)+". "+ obj.firstName + obj.lastName+": ";
        console.log(orang);
        console.log(obj);
        
    }
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

//soal 2
console.log (batas+'\n'+'Soal 2'+'\n'+batas)

function shoppingTime(memberId,money) {
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money<50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        var sisaMoney= money;
        var list = []
        var newObj = {}
        
         for(i=0;sisaMoney>=50000;i++) {
             if (sisaMoney>=1500000) {
                 list.push ("Sepatu Stacattu")
                 sisaMoney -= 1500000
             } else if (sisaMoney>=500000) {
                 list.push ("Baju Zoro")
                 sisaMoney -=500000
             } else if (sisaMoney>=250000){
                 list.push ("Baju H&N")
                 sisaMoney -=250000
             } else if (sisaMoney>=175000){
                 list.push ("Sweater Uniklooh")
                 sisaMoney -=175000
             } else if (sisaMoney>=50000){
                 list.push("Casing Handphone")
                 sisaMoney -= 50000
             }
         }
         newObj.memberId = memberId
         newObj.money = money
         newObj.listPurcahsed = list
         newObj.changeMoney = sisaMoney
         return newObj
    }

}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


// soal 3
console.log (batas+'\n'+'Soal 2'+'\n'+batas)

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    if(arrPenumpang.length <=0){
        return[]
    }

    var arrHasil = []
    for ( i = 0;i< arrPenumpang.length;i++){
    var objAngkot = {}
    var asal = arrPenumpang [i][1]
    var tujuan = arrPenumpang [i][2]

    var indexAsal;
    var indexTujuan;

    for(j=0;j<rute.length;j++){
            if(rute[j]==asal){
                indexAsal =j
            } else if (rute[j]==tujuan){
                indexTujuan= j
            }
        }
    var bayar = (indexTujuan-indexAsal)*2000

    objAngkot.penumpang = arrPenumpang [i][0]
    objAngkot.naikDari = asal
    objAngkot.tujuan = tujuan
    objAngkot.bayar = bayar

    arrHasil.push(objAngkot)
    }
    return  arrHasil
  }

  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  console.log(naikAngkot([]));