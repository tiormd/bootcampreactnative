var batas = "========================="
console.log (batas+'\n'+'Soal 1'+'\n'+batas)
// soal 1
const golden = goldenFunction = () => {
    console.log("this is golden!!")
}

golden()

console.log (batas+'\n'+'Soal 2'+'\n'+batas)

const newFunction = function literal(firstName, lastName) {
    return {firstName, 
        lastName, 
        fullName:  function(){
            console.log(firstName + " " + lastName)
            return 
        }
        } 
}
newFunction("William", "Imoh").fullName()

console.log (batas+'\n'+'Soal 3'+'\n'+batas)

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName, lastName, destination, occupation, spell} = newObject 
  console.log(firstName, lastName, destination, occupation)

console.log (batas+'\n'+'Soal 4'+'\n'+batas)

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)

console.log (batas+'\n'+'Soal 5'+'\n'+batas)
// sooal 5

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before) 
